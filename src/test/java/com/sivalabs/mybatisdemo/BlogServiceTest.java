package com.sivalabs.mybatisdemo;


import org.junit.FixMethodOrder;
import java.util.List;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.sivalabs.mybatisdemo.domain.Blog;
import com.sivalabs.mybatisdemo.service.BlogService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BlogServiceTest {

	private static BlogService blogService;

	@BeforeClass
	public static void setUp() {
		blogService = new BlogService();
	}

	@AfterClass
	public static void tearDown() {
		blogService = null;
	}

	@Test
	public void testBGetBlogById() {
		Blog blog = blogService.getBlogById(2);

		Assert.assertNotNull(blog);
		System.out.println("testing getBlogById: " + blog);
	}

	@Test
	public void testAInsertBlog() {
		Blog blog = new Blog();
		blog.setBlogId(1);
		blog.setBlogName("testName1");
		blog.setCreatedOn(new Date());
		blogService.insertBlog(blog);

		Assert.assertTrue(blog.getBlogId() != 0);
		Blog createdBlog = blogService.getBlogById(blog.getBlogId());
		Assert.assertNotNull(createdBlog);
		Assert.assertEquals(blog.getBlogName(), createdBlog.getBlogName());
	}

	@Test
	public void testCGetAllBlogs() {
		List<Blog> blogs = blogService.getAllBlogs();

		Assert.assertNotNull(blogs);
		System.out.println("testing getAllBlogs: " + blogs);
	}

	@Test
	public void testDUpdateBlog() {
		long timestamp = System.currentTimeMillis();
		Blog blog = blogService.getBlogById(2);
		blog.setBlogName("TestBlogName" + timestamp);
		blogService.updateBlog(blog);

		Blog updatedBlog = blogService.getBlogById(2);
		Assert.assertEquals(blog.getBlogName(), updatedBlog.getBlogName());
	}

	@Test
	public void testCDeleteBlog() {
		Blog blog = blogService.getBlogById(4);
		blogService.deleteBlog(blog.getBlogId());
		
		Blog deletedBlog = blogService.getBlogById(4);
		Assert.assertNull(deletedBlog);
	}

}
