package com.sivalabs.mybatisdemo.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.sivalabs.mybatisdemo.domain.Blog;
import com.sivalabs.mybatisdemo.mappers.BlogMapper;

public class BlogService implements BlogMapper {

	@Override
	public void insertBlog(Blog blog) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		
		try{
			BlogMapper blogMapper = session.getMapper(BlogMapper.class);
			blogMapper.insertBlog(blog);
			session.commit();
		} catch (Exception e) {
			e.getStackTrace();
			session.rollback();
		} finally {
			session.close();
		}		
	}

	@Override
	public Blog getBlogById(Integer blogId) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		
		try{
			BlogMapper blogMapper = session.getMapper(BlogMapper.class);
			return blogMapper.getBlogById(blogId);
		} catch (Exception e) {
			e.getStackTrace();
			session.rollback();
			return null;
		} finally {
			session.close();
		}		
	}

	@Override
	public List<Blog> getAllBlogs() {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		
		try{
			BlogMapper blogMapper = session.getMapper(BlogMapper.class);
			return blogMapper.getAllBlogs();
		} catch (Exception e) {
			e.getStackTrace();
			session.rollback();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public void updateBlog(Blog blog) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		
		try{
			BlogMapper blogMapper = session.getMapper(BlogMapper.class);
			blogMapper.updateBlog(blog);
			session.commit();
		} catch (Exception e) {
			e.getStackTrace();
			session.rollback();
		} finally {
			session.close();
		}
		
		
	}

	@Override
	public void deleteBlog(Integer blogId) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		
		try{
			BlogMapper blogMapper = session.getMapper(BlogMapper.class);
			blogMapper.deleteBlog(blogId);
			session.commit();
		} catch (Exception e) {
			e.getStackTrace();
			session.rollback();
		} finally {
			session.close();
		}
		
	}

}
